# CorrAMR

Correlation-based Assay of Metagenomic community Relationships 
through simple correlations, compositionally-aware network inference methods 
and statistical test for identifying differentially abundant taxa.

This repository contains the scripts used for my Master Thesis work. It consists
of two main scripts: function.R contains all the useful functions for performing
downstream analysis; run_analysis calls the function.R scripts and run all the
methods in series to obtain the result presented in my thesis.

All the analysis has been run on data sets from the R package Curated Metagenomic Data.

[Pasolli E, Schiffer L, Manghi P, Renson A, Obenchain V, Truong D, Beghini F, Malik F, 
Ramos M, Dowd J, Huttenhower C, Morgan M, Segata N, Waldron L (2017). 
“Accessible, curated metagenomic data through ExperimentHub.” 
Nat. Methods, 14(11), 1023–1024. ISSN 1548-7091, 1548-7105, doi: 10.1038/nmeth.4468.]

## Simple Correlation (Pearson and Spearman indeces)

## Bootstrapped-Wilcoxon test to identify differentially expressed taxa in Prevotella copri presence/absence conditions

## Compositionality-aware Network Inference Methods for defining a consensus network to compare with results obtained by simple correlation

### Spearman
### CCrepe

[Faust, K., Sathirapongsasuti, J.F., Izard, J., Segata, N., Gevers, D., Raes, J., and Huttenhower, C. (2012). 
Microbial co-occurrence relationships in the Human Microbiome. PLoS Computational Biology 8.]

### SparCC

[Friedman, J., and Alm, E.J. (2012). Inferring Correlation Networks from Genomic Survey Data. PLoS Computational Biology 8.]

### CCLasso

[Fang, H., Huang, C., Zhao, H., and Deng, M. (2015). CCLasso: Correlation inference for compositional data through Lasso. Bioinformatics 31, 3172–3180.]

### REBACCA

[Ban, Y., An, L., and Jiang, H. (2015). Investigating microbial co-occurrence patterns based on metagenomic compositional data. Bioinformatics 31, 3322–3329.]

## How to run the code

Open the `run_analysis.R` script in RStudio and run all the code or, alternatively, launch from terminal with the command `RScript run_analysis.R`

